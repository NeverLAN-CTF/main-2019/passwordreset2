<!DOCTYPE html>
<html>
    <head>
    <title>reset your password</title>
    <style>
        body{
			text-align:center;
			background-color:#f0f4f4;
	    }
	    .loginBox{
	    	width: 30%;
		    margin: auto;
		    min-height: 10rem;
		    padding: 1rem;
		    background-color: white;
	    }
	    input{
	    	display: block;
		    width: 75%;
		    margin: .5rem auto 1rem;
	    }
    </style>
    </head>
    <body>
        <div class="loginBox">
            <?php
            require_once('db.php');
            if(array_key_exists('passed', $_COOKIE) &&
               array_key_exists('username', $_COOKIE)){
                if($_COOKIE['passed'] == 'true'){
                    echo('
                        <h3>Pick The New Password</h3>
                        <form action="reset4.php" method="GET">
                        <label for="new_pass">New Password</label>
                        <input name="new_pass" type="text"/>
                        <label for="new_pass2">Repeat Password</label>
                        <input name="new_pass2" type="text"/>
                        <input type="submit"/>
                        </form>
                    ');
                }else{
                    echo("<h3>Sorry, It looks like You missed a security question</h3><br><a href='reset1.php'>Try Again?</a>");
                }
            }
            ?>
        </div>
    </body>
</html>
